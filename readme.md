# Lit Paths

This is a simple project to implement node.js path API into a browser, as cleanly as possible.

### install

```bash
npm i -S @acce/lit-path
```

### use

```js
import * as path from '@acce/lit-path';

path.dirname('/test/directory.ext'); //> "/test"
```

or

```js
import { basename } from '@acce/lit-path';

path.basename('/test/directory.ext', '.ext'); //> "directory"
```

## Supported APIs

This implements near 1-1 code with node.js path API.

Here are the implemented APIs

* path.basename
* path.dirname
* path.extname
* path.normalize
* path.join
* path.resolve
* path.parse
* path.format

#### differences

* `process.cwd()` is assumed to be the `location.pathname`
* path format is always posix, because the web...
