const errs = [];

class AssertionError extends Error {
	constructor(message) {
		super('AssertionError: ' + message);
		this.name = 'AssertionError';
		errs.push(this);
	}
}

window.objectEquals = function objectEquals(x, y) {
	if (x === null || x === undefined || y === null || y === undefined) { return x === y; }

	// after this just checking type of one would be enough
	if (x.constructor !== y.constructor) { return false; }

	// if they are functions, they should exactly refer to same one (because of closures)
	if (x instanceof Function) { return x === y; }

	// if they are regexps, they should exactly refer to same one (it is hard to better equality check on current ES)
	if (x instanceof RegExp) { return x === y; }

	if (x === y || x.valueOf() === y.valueOf()) { return true; }
	if (Array.isArray(x) && x.length !== y.length) { return false; }

	// if they are dates, they must had equal valueOf
	if (x instanceof Date) { return false; }

	// if they are strictly equal, they both need to be object at least
	if (!(x instanceof Object)) { return false; }
	if (!(y instanceof Object)) { return false; }

	// recursive object equality check
	var p = Object.keys(x);
	return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) &&
		p.every(function (i) { return objectEquals(x[i], y[i]); });
}

export const equal = (value, expectedValue) => {
	if (!objectEquals(value, expectedValue)) {
		throw new AssertionError(`Expected value was not found.
	Expected ${JSON.stringify(expectedValue)}
	but got ${JSON.stringify(value)}`);
	}
}

export const throws = (method) => {
	try {
		const val = method();

		throw new AssertionError(`Expected method to throw, but it didn\'t
	method returned ${JSON.stringify(val)}`)
	}

	catch (e) {
		if (e instanceof AssertionError) throw e;
		//	all G
	}
}

const colors = {
	failed: "background: red; color: white; font-size: small",
	passed: "background: green; color: white; font-size: small",
}

const logged = (message, color) => console.log('%c' + ' ' + message + ' ', color);
const passed = (message) => logged(message, colors.passed);
const failed = (message) => logged(message, colors.failed);

export const complete = () => {
	if (!errs.length) return passed(`Tests have passed! There were ${errs.length} errors.`);


	failed(`Tests have not passed! There were ${errs.length} errors.`);
	errs.forEach(e => console.error(e))
}
